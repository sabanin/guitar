package guitar;

public class Guitar {
	private MidiGuitar midiGuitar;  
	
	public Guitar(MidiGuitar midiGuitar){
		this.midiGuitar = midiGuitar;
	}
	
	public void play(Tab tab) throws Exception{
		midiGuitar.play(tab.notesToPlay());
	}

}
