package guitar;

import java.util.HashMap;
import java.util.Map;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MetaEventListener;
import javax.sound.midi.MetaMessage;
import javax.sound.midi.MidiEvent;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.Sequence;
import javax.sound.midi.Sequencer;
import javax.sound.midi.ShortMessage;
import javax.sound.midi.Track;


/**
 * This class interfaces the default synthesizer via the
 * javax.sound.midi library.
 * 
 * The code is based in the class CreateSequence.java
 * by Matthias Pfisterer, accessible at
 * 
 * http://www.jsresources.org/examples/CreateSequence.java.html
 * 
 * Please see the corresponding Copyright, in the file 'copyright.txt'
 * in the root directory of this eclipse project.
 */
public class MidiGuitar {
	private static final int	VELOCITY = 64;
	
	@SuppressWarnings("serial")
	private Map<String, Integer> EADGBe = new HashMap<String, Integer>(){{
		put("e", 64);
		put("B", 59);
		put("G", 55);
		put("D", 50);
		put("A", 45);
		put("E", 40);
	}};
	
	public void play(String input) throws Exception {
		Sequence sequence = new Sequence(Sequence.PPQ, 1);
		Track	track = sequence.createTrack();

		int pos = 0;
		for (String chord: input.split(" ")) {
			for (String note: chord.split("/")) {
				String str = note.substring(0, 1);
				if (!str.equals("_")) {
					Integer fret = Integer.parseInt(note.substring(1));
					track.add(createNoteOnEvent(EADGBe.get(str) + fret, pos));
					track.add(createNoteOffEvent(EADGBe.get(str) + fret, pos + 1));
				} else
					track.add(createNoteOffEvent(50, pos + 1));
			}
			pos++;
		}
		
		final Sequencer sequencer = MidiSystem.getSequencer();
		sequencer.addMetaEventListener(new MetaEventListener() {				
			public void meta(MetaMessage event)	{
				if (event.getType() == 47) {
					sequencer.close();
				}
			}
		});
		
		sequencer.open();
		sequencer.setSequence(sequence);
		
		sequencer.start();
	}

	private MidiEvent createNoteOnEvent(int nKey, long lTick) {
		return createNoteEvent(ShortMessage.NOTE_ON, nKey, VELOCITY, lTick);
	}

	private MidiEvent createNoteOffEvent(int nKey, long lTick) {
		return createNoteEvent(ShortMessage.NOTE_OFF, nKey, 0, lTick);
	}

	private MidiEvent createNoteEvent(int nCommand, int nKey, int nVelocity, long lTick) {
		ShortMessage	message = new ShortMessage();
		try {
			message.setMessage(nCommand,
							   0,	// always on channel 1
							   nKey,
							   nVelocity);
		}
		catch (InvalidMidiDataException e) {
			e.printStackTrace();
			System.exit(1);
		}
		MidiEvent	event = new MidiEvent(message, lTick);
		return event;
	}
	
	public static void main(String[] args) throws Exception {
		MidiGuitar guitar = new MidiGuitar();
//		guitar.play("e12 B12 e14 B12 e15 B12 e17 B12 e8 B5 G5 e8 B5 G5 G5 B5 e8 B5 G5 G0 e0 B1 G0 e0 B1 e0 B3 G2 e0 B3");
//		guitar.play("D7 G5 B5 e5 e7/D6 B5 G5 e7 e8/D5 B5 G5 e8 e2 B4 G2 e2 e0 B1 G2 B1 e0 B1 G2 A2 A0 A0 A0 A8 A7");
//		guitar.play("B5 B5 B6 B8 B8 B6 B5 B3 B1 B1 B3 B5 B5 B3 B3");
//		guitar.play("D12 B15 G14 G12 e15 G14 e14 G14 D12 B15 G14 G12 e15 G14 e14 G14 D14 B15 G14 G12 e15 G14 e14 G14 D14 B15 G14 G12 e15 G14 e14 G14 G12 B15 G14 G12 e15 G14 e14 G14 G12 B15 G14 G12 e15 G14 e14 G14");
//		guitar.play("D7 G5 B5 e5 e7/D6 B5 G5 e7 e8/D5 B5 G5 e8 e2/D4 B3 G2 e2 e0/D3 B1 G2 B1 _ e0 B1 G2 B0/G0/A2 B1/G2/A0 B1/G2/A0 _ A0 _ A8 A7 A0 _ D7 G5 B5 e5 e7/D6 B5 G5 e7 e8/D5 B5 G5 e8 e2/D4 B3 G2 e2 e0/D3 B1 G2 B1 _ e0 B1 G2 B0/G0/A2 B1/G2/A0 B1/G2/A0 _ _ _ A0 A2");
//		guitar.play("D7 G5 B5 e5 e7 B5 G5 e7 e8 B5 G5 e8 e2 B3 G2 e2 e0 B1 G2 B1 e0 e0 B1 G2 B0 B1 B1");
		guitar.play("E3 D0 G2 D0 B0 G2 D0 G2 E3 D0 G2 D0 B0 G2 D0 G2 E0 A2 D4 A2 G0 D4 A2 D4");
	}
}
