package guitar;

import static org.mentaregex.Regex.*;

public class Tab {
	private String tab;
	private final String strings = "eBGDAE";

	public Tab(String tab) {
		this.tab = tab;
	}

	public String notesToPlay() {
		String result = new String();
		// boolean firstIteration = true;
		// for (String fret : match(tab, "/(\\d+)/g")) {
		// if (!firstIteration)
		// result += " ";
		// result += "B" + fret;
		// firstIteration = false;
		// }
		String lines[] = tab.split("\n");
		for (int i = 0; i < lines.length; i+=7) {
			for (int j = 1; j < lines[i].length(); j += 2) {
				char[] column = { lines[i].charAt(j), lines[i + 1].charAt(j),
						lines[i + 2].charAt(j), lines[i + 3].charAt(j),
						lines[i + 4].charAt(j), lines[i + 5].charAt(j) };
				String chord = makeChord(column);
				if ((!result.isEmpty()) && (!chord.isEmpty())) result += " ";
				result += chord;
			}
		}
		return result;
	}

	private String makeChord(char notes[]) {
		String result = new String();
		boolean isFirst = true;
		if (notes[0] == '|')
			return result;
		for (int i = 0; i < notes.length; i++)
			if (Character.isDigit(notes[i])) {
				if (!isFirst)
					result += "/";
				result += Character.toString(strings.charAt(i)) + Character.toString(notes[i]);
				isFirst = false;
			}
		if (isFirst) return "_";
		return result;
	}
}