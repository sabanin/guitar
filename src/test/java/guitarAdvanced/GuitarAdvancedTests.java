package guitarAdvanced;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import guitar.Guitar;
import guitar.MidiGuitar;
import guitar.Tab;

import org.junit.Test;

public class GuitarAdvancedTests {

	@Test
	public void test1() throws Exception{
		String tabstr = "e|-------5-7-----7-|\n"
				+ "B|-----5-----5-----|\n"
				+ "G|---5---------5---|\n"
				+ "D|-7-------6-------|\n"
				+ "A|-----------------|\n"
				+ "E|-----------------|\n";
		String expNotes = "D7 G5 B5 e5 e7/D6 B5 G5 e7";
		MidiGuitar midiGuitar = mock(MidiGuitar.class);
		Guitar guitar = new Guitar(midiGuitar);
		guitar.play(new Tab(tabstr));
		verify(midiGuitar).play(expNotes);
	}
	
	@Test
	public void test2() throws Exception{
		String tabstr = "e|-------5-7-----7-|-8-----8-2-----2-|-0---------0-----|-----------------|\n"
				+ "B|-----5-----5-----|---5-------3-----|---1---1-----1---|-0-1-1-----------|\n"
				+ "G|---5---------5---|-----5-------2---|-----2---------2-|-0-2-2-----------|\n"
				+ "D|-7-------6-------|-5-------4-------|-3---------------|-----------------|\n"
				+ "A|-----------------|-----------------|-----------------|-2-0-0---0---8-7-|\n"
				+ "E|-----------------|-----------------|-----------------|-----------------|\n";
		String expNotes = "D7 G5 B5 e5 e7/D6 B5 G5 e7 e8/D5 B5 G5 e8 e2/D4 B3 G2 e2 e0/D3 B1 G2 B1 _ e0 B1 G2 B0/G0/A2 B1/G2/A0 B1/G2/A0 _ A0 _ A8 A7";
		MidiGuitar midiGuitar = mock(MidiGuitar.class);
		Guitar guitar = new Guitar(midiGuitar);
		guitar.play(new Tab(tabstr));
		verify(midiGuitar).play(expNotes);
	}
	
	@Test
	public void test3() throws Exception{
		String tabstr = "e|-------5-7-----7-|-8-----8-2-----2-|-0---------0-----|-----------------|\n"
				+ "B|-----5-----5-----|---5-------3-----|---1---1-----1---|-0-1-1-----------|\n"
				+ "G|---5---------5---|-----5-------2---|-----2---------2-|-0-2-2-----------|\n"
				+ "D|-7-------6-------|-5-------4-------|-3---------------|-----------------|\n"
				+ "A|-----------------|-----------------|-----------------|-2-0-0---0---8-7-|\n"
				+ "E|-----------------|-----------------|-----------------|-----------------|\n"
				+ "\n"
				+ "e|-----------7-----7-|-8-----8-2-----2-|-0---------0-----|-----------------|\n"
				+ "B|---------5---5-----|---5-------3-----|---1---1-----1---|-0-1-1-----------|\n"
				+ "G|-------5-------5---|-----5-------2---|-----2---------2-|-0-2-2-----------|\n"
				+ "D|-----7-----6-------|-5-------4-------|-3---------------|-----------------|\n"
				+ "A|-0-----------------|-----------------|-----------------|-2-0-0-------0-2-|\n"
				+ "E|-------------------|-----------------|-----------------|-----------------|\n";
		
		String expNotes = "D7 G5 B5 e5 e7/D6 B5 G5 e7 e8/D5 B5 G5 e8 e2/D4 B3 G2 e2 e0/D3 B1 G2 B1 _ e0 B1 G2 B0/G0/A2 B1/G2/A0 B1/G2/A0 _ A0 _ A8 A7 A0 _ D7 G5 B5 e7/D6 B5 G5 e7 e8/D5 B5 G5 e8 e2/D4 B3 G2 e2 e0/D3 B1 G2 B1 _ e0 B1 G2 B0/G0/A2 B1/G2/A0 B1/G2/A0 _ _ _ A0 A2";
		MidiGuitar midiGuitar = mock(MidiGuitar.class);
		Guitar guitar = new Guitar(midiGuitar);
		guitar.play(new Tab(tabstr));
		verify(midiGuitar).play(expNotes);
	}
}
