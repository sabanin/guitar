package guitarBeginner;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;  
import guitar.Guitar;
import guitar.MidiGuitar;
import guitar.Tab;
import cucumber.api.PendingException;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class GuitarBeginnerSteps {
	
	Guitar guitar = null;
	Tab tab = null;
	MidiGuitar midiGuitar = null;

	//Beginners scenario
	
	@Before
	public void setUp(){
		midiGuitar = mock(MidiGuitar.class);
	}
	
	@Given("^I start my application$")
	public void I_start_my_application() throws Throwable {
	    guitar = new Guitar(midiGuitar);
	}

	@Given("^I load the following guitar tab$")
	public void I_load_the_following_guitar_tab(String tab) throws Throwable {
	    this.tab = new Tab(tab);
	}

	@When("^the guitar plays$")
	public void the_guitar_plays() throws Throwable {
	    guitar.play(tab);
	}

	@Then("^the following notes should be played$")
	public void the_following_notes_should_be_played(String expectation) throws Throwable {
	    verify(midiGuitar).play(expectation);
	}
	
}
