package guitarBeginner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import guitar.Guitar;
import guitar.MidiGuitar;
import guitar.Tab;

import org.junit.Test;

public class GuitarBeginnerTests {

	@Test
	public void testTab1() throws Exception {
		String tabstr = "e|-------------------------------|\n"
				+ "B|-5-5-6-8-8-6-5-3-1-1-3-5-5-3-3-|\n"
				+ "G|-------------------------------|\n"
				+ "D|-------------------------------|\n"
				+ "A|-------------------------------|\n"
				+ "E|-------------------------------|\n";
		String expNotes = "B5 B5 B6 B8 B8 B6 B5 B3 B1 B1 B3 B5 B5 B3 B3";
		MidiGuitar midiGuitar = mock(MidiGuitar.class);
		Guitar guitar = new Guitar(midiGuitar);
		guitar.play(new Tab(tabstr));
		verify(midiGuitar).play(expNotes);
	}

	@Test
	public void testTab2() throws Exception {
		String tabstr = "e|-------------------------------|\n"
				+ "B|-5-5-6-8-8-6-5-3-1-1-3-5-3-1-1-|\n"
				+ "G|-------------------------------|\n"
				+ "D|-------------------------------|\n"
				+ "A|-------------------------------|\n"
				+ "E|-------------------------------|\n";
		String expNotes = "B5 B5 B6 B8 B8 B6 B5 B3 B1 B1 B3 B5 B3 B1 B1";
		MidiGuitar midiGuitar = mock(MidiGuitar.class);
		Guitar guitar = new Guitar(midiGuitar);
		guitar.play(new Tab(tabstr));
		verify(midiGuitar).play(expNotes);
	}
	
}
