package guitarBeginner;

import static org.junit.Assert.*;
import static org.hamcrest.core.Is.is;
import guitar.Tab;

import org.junit.Assert;
import org.junit.Test;

public class TabBeginnerTests {

	@Test
	public void test1() {
		String tabstr = "e|-------------------------------|\n"
				+ "B|-5-5-6-8-8-6-5-3-1-1-3-5-5-3-3-|\n"
				+ "G|-------------------------------|\n"
				+ "D|-------------------------------|\n"
				+ "A|-------------------------------|\n"
				+ "E|-------------------------------|\n";
		String expNotes = "B5 B5 B6 B8 B8 B6 B5 B3 B1 B1 B3 B5 B5 B3 B3";
		Tab tab = new Tab(tabstr);
		
		Assert.assertThat(tab.notesToPlay(), is(expNotes));
	}
	
	@Test
	public void test2() {
		String tabstr = "e|-------------------------------|\n"
				+ "B|-5-5-6-8-8-6-5-3-1-1-3-5-3-1-1-|\n"
				+ "G|-------------------------------|\n"
				+ "D|-------------------------------|\n"
				+ "A|-------------------------------|\n"
				+ "E|-------------------------------|\n";
		String expNotes = "B5 B5 B6 B8 B8 B6 B5 B3 B1 B1 B3 B5 B3 B1 B1";
		Tab tab = new Tab(tabstr);
		
		Assert.assertThat(tab.notesToPlay(), is(expNotes));
	}
	
}
