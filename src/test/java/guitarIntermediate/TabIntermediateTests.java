package guitarIntermediate;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;
import guitar.Tab;

import org.junit.Assert;
import org.junit.Test;

public class TabIntermediateTests {

	@Test
	public void test1() throws Exception{
		String tabstr = "e|-7-----7-----7-----7-----5-----3-----3-----2-----0-----0-----|\n"
				+ "B|---0-----0-----0-----0-----0-----0-----0-----0-----0-----0---|\n"
				+ "G|-----0-----0-----0-----0-----0-----0-----0-----0-----0-----0-|\n"
				+ "D|-------------------------------------------------------------|\n"
				+ "A|-------------------------------------------------------------|\n"
				+ "E|-------------------------------------------------------------|\n";
		String expNotes = "e7 B0 G0 e7 B0 G0 e7 B0 G0 e7 B0 G0 e5 B0 G0 e3 B0 G0 e3 B0 G0 e2 B0 G0 e0 B0 G0 e0 B0 G0";
		Tab tab = new Tab(tabstr);
		
		Assert.assertThat(tab.notesToPlay(), is(expNotes));
	}
	
	@Test
	public void test2() throws Exception{
		String tabstr = "e|-------5-7-----7-8-----8-2-----2-0-------0-0-----------|\n"
				+ "B|-----5-----5-------5-------3-------1---1-----1---0-1-1-|\n"
				+ "G|---5---------5-------5-------2-------2---------2-------|\n"
				+ "D|-7-----------------------------------------------------|\n"
				+ "A|-------------------------------------------------------|\n"
				+ "E|-------------------------------------------------------|\n";
		String expNotes = "D7 G5 B5 e5 e7 B5 G5 e7 e8 B5 G5 e8 e2 B3 G2 e2 e0 B1 G2 B1 e0 e0 B1 G2 B0 B1 B1";
		Tab tab = new Tab(tabstr);
		
		Assert.assertThat(tab.notesToPlay(), is(expNotes));
	}
}
